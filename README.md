# Real Time Reporting

Real Time Reporting project with Highcharts, AngularJS and SpringBoot

`mvn clean spring-boot:run`

`http://localhost:8080/`

![Real Time Reporting](img/home.png)