package com.hendisantika.rest;

import com.hendisantika.dto.ChartDataDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * Project : real-time-reporting
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/02/18
 * Time: 19.18
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/real-time-data")
public class RealTimeDataResource {

    @GetMapping(value = "/data")
    public ResponseEntity<Object> getData() {
        return ResponseEntity.ok(new ChartDataDTO(System.currentTimeMillis(), new Random().nextInt(100)));
    }
}
