package com.hendisantika.dto;

/**
 * Created by IntelliJ IDEA.
 * Project : real-time-reporting
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/02/18
 * Time: 19.15
 * To change this template use File | Settings | File Templates.
 */
public class ChartDataDTO {

    private Long xAxis;
    private Integer yAxis;

    public ChartDataDTO(Long xAxis, Integer yAxis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    public Long getxAxis() {
        return xAxis;
    }

    public Integer getyAxis() {
        return yAxis;
    }
}
