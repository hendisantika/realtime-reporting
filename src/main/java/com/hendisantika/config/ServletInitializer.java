package com.hendisantika.config;

import com.hendisantika.RealTimeReportingApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Created by IntelliJ IDEA.
 * Project : real-time-reporting
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/02/18
 * Time: 19.13
 * To change this template use File | Settings | File Templates.
 */

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(RealTimeReportingApplication.class);
    }
}
